import { Router } from 'express';

import { appContainer } from '../inversify.config';
import { HealthController } from '../server/controllers/health.controller';
import { SwaggerController } from '../server/controllers/swagger.controller';

const routes = Router();

const swaggerController = appContainer.get<SwaggerController>(
  nameof<SwaggerController>()
);
const healthController = appContainer.get<HealthController>(
  nameof<HealthController>()
);

routes.get('/', healthController.getHealth.bind(healthController));

routes.get('/health', healthController.getHealth.bind(healthController));

routes.get('/api', swaggerController.getDocs.bind(swaggerController));

export { routes };
