import { Config } from '../server/models/config';

export const productionConfig: Config = {
  serviceConfig: {
    name: 'Template API Microservice',
    environment: 'production',
    namespace: 'template-api',
    host: 'production.template-api.api.findnemo.in',
    description: 'Template to be used for microservices',
    port: 5000,
  },
  googleCloudConfig: {
    projectId: 'vishvajit2',
    secrets: {
      database: 'prod-db',
      firebase: 'firebase-service-account',
      directionapikey: 'direction-api',
    },
    firestoreCollection: 'productionTimestamp',
  },
  endpoints: {
    userApi: 'https://production.template-api.api.findnemo.in',
    deviceApi: 'https://production.device-api.api.findnemo.in',
  },
};
