import { Config } from '../server/models/config';

export const developmentConfig: Config = {
  serviceConfig: {
    name: 'Template API Microservice',
    environment: 'development',
    namespace: 'template-api',
    host: 'staging.template-api.api.findnemo.in',
    description: 'Template to be used for microservices',
    port: 5000,
  },
  googleCloudConfig: {
    projectId: 'vishvajit2',
    secrets: {
      database: 'dev-db',
      firebase: 'firebase-service-account',
      directionapikey: 'direction-api',
    },
    firestoreCollection: 'stagingTimestamp',
  },
  endpoints: {
    userApi: 'https://staging.template-api.api.findnemo.in',
    deviceApi: 'https://staging.device-api.api.findnemo.in',
  },
};
